# UTS-Praktikum PBO

# No 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)

[penyelesaian masalah dengan pendekatan matematika](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java)

pendekatan matematika pada program ini saya kira terdapat pada method prev() dan method next(), disana digunakan pendekatan matematika agar program bisa mendapatkan index yang tepat dan sesuai. 

# No 2
Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

1. **masalah:** 
    ketika ingin mengakses next konten namun index konten berada di bagian akhir

    **solusi:**
ketika index mencapai bagian akhir, nilai index akan diubah menjadi 0. 

    **algoritma:**

    1. periksa index konten saat ini
    2. jika index konten != list.size()-1, ke langkah 4
    3. ubah nilai index menjadi 0
    4. putar konten selanjutnya

    **link:** _[method next() pada class MusicPlayer](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L90); [method next() pada class PodcastPlayer](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L174)_
    
2. **masalah:**
ketika ingin mengakses previous konten namun index konten berada di bagian awal
    
    **solusi:**
ketika index mencapai bagian awal, nilai index akan diubah menjadi list.size()-1. 

    **algoritma:**
    1. periksa index konten saat ini
    2. jika index konten != 0, ke langkah 4
    3. ubah nilai index menjadi list.size()-1
    4. putar konten sebelumnya

    **link:** _[method prev() pada class MusicPlayer](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L110); [method prev() pada class PodcastPlayer](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L203)_

3. **masalah:**
user menginput menu play padahal konten sedang berada dalam posisi play/user menginput menu pause sedangkan konten berada dalam posisi pause. 
    
    **solusi:**
    
    buat sebuah variable yang merepresentasikan kondisi pemutar konten saat ini. Dalam program dicontohkan dengan variabl [lastCondition](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L151) pada class data. Karena lastCondition menerapkan polymorphism maka untuk mengaksesnya memerlukan method: getCondition(); dan setCondition(); 

    ketika user mengakses menu play, variable condition akan diinisialisasi ulang ke nilai "play". Begitupun dengan pause, jika user mengakses menu pause, maka variable condition akan diinisialisasi ulang ke nilai "pause". Menu yang ditampilkan hanya salah satu dari pause/play sesuai dengan kondisi yang tadi di inisialisasikan. Jadi kedua menu tersebut tidak akan ditampilkan dalam waktu yang bersamaan
    
    **algoritma:**
    1. periksa variable condition pada class Data
    2. apabila condition == "play", ke langkah 4
    3. apabila condition == "pause", ke langkah 6
    4. tampilkan menu pause, set condition menjadi "pause"
    5. ke langkah 7
    6. tampilkan menu play, set condition menjadi "play"
    7. melakukan proses selanjutnya

    link: _[optionPlayer()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Menu.java#L45)_

# No 3
Mampu menjelaskan konsep dasar OOP

OOP merupakan cara penulisan kode program yang mirip dengan cara kita berinteraksi dengan benda-benda dalam kehidupan sehari-hari. Dalam OOP, kita memandang setiap objek dalam dunia nyata sebagai objek dalam program. Kemudian kita menuliskan kode program untuk merepresentasikan objek-objek tersebut agar dapat berinteraksi dan bekerja bersama-sama. 

OOP sangat populer dan banyak digunakan pada saat ini, karena dengan menggunakan OOP kode program akan lebih terstruktur dan mudah dipahami serta mudah dipelihara. Tujuan penggunaan OOP ini juga agar lebih mudah dalam mengelola program dan tentunya program akan menjadi lebih efisien.

Konsep dasar OOP:
1. Enkapsulasi: merupakan prinsip pada OOP yang menggabungkan atribut dan metode ke dalam sebuah objek kemudian menyembunyikan detail implementasinya dari luar objek. 
2. Inheritance: merupakan prinsip pada OOP yang memungkinkan sebuah kelas dapat mewarisi atribut dan metode dari kelas lain. Dengan menggunakan inheritance kita dapat  membuat sebuah kelas baru yang memiliki atribut dan metode dari kelas  yang sudah ada. 
3. Polymorphism merupakan prinsip pada OOP yang memungkinkan sebuah objek memiliki banyak bentuk/perilaku. 
4. Abstraction: merupakan prinsip dalam OOP yang mengacu pada kemampuan untuk menyembunyikan detail implementasi yang kompleks dari penggunaan sebuah objek dan hanya menampilkan informasi yang diperlukan untuk melakukan tugas tertentu. 

# No 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

[Encapsulation](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java)

Enkapsulasi disini dilakukan untuk melindungi data-data/variabel dalam program agar tidak bisa sembarangan diakses dari luar class. Biasanya menggunakan modifier private dan untuk mengakses data/variabel nya digunakan method getter dan setter. 

# No 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat  (Lampirkan link source code terkait)

[Abstraction](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L15)

class Player dijadikan sebagai abstract karena class ini akan menjadi kerangka/template bagi subclassnya. Hal ini akan memastikan bahwa si subclass atau kelas turunannya memiliki method yang sama dengan superclassnya. Dalam contoh ini terletak pada class MusicPlayer dan PodcastPlayer yang keduanya merupakan subclass dari class Player. 

# No 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

**Inheritance**
- **superclass:**
_[class Player](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L15)_
- **subclass:** 
_[class MusicPlayer](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L45);
[class PoscastPlayer](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L125)_
- **line:** _15-222_
- **link:** [Inheritance class Features](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java)

- **superclass:**
_[class Content](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L29)_
- **subclass:** 
_[class Music](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L62); [class Podcast](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L84)_
- **line:** _29-101_
- **link:** _[Inheritance class DataObj](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java)_

kedua class ini menerapkan inheritance karena keduanya memiliki method-method yang hampir sama dan untuk mempermudah pengelolaan kode dan agar lebih efisien maka keduanya dijadikan subclass dari method superclassnya.

**Polymorphism**

- **override**
    - Method pada class MusicPlayer yang meng-Override method dari [class Player](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L15): _method recent(); method play(); method pause(); method next(); method prev()_
    
        line: 50-123

        link: [Override pada Class MusicPlayer](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L50)
    - Method pada class PodcastPlayer yang meng-Override method dari class [class Player](https://gitlab.com/rihapfirdaus/spotifai/-/blob/e2029ab553987d14544260892c92bc71a8a5d932/Features.java#L15): _method recent(); method play(); method pause(); method next(); method prev()_

        line: 130-221

        link: [Override pada Class PodcastPlayer](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L130)

    method-method disini di override untuk mengubah implementasi method yang terdapat pada superclass untuk dijadikan lebih spesifik lagi pada method turunannya. 
- **overloading**

    Method yang menerapkan overloading:
    
    - Method setMusic() pada class Data

        [**setMusic()**](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L163)
        
        [**setMusic(String tittle, String artist, String genre, String album)**](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L220)
        
        overloading method ini terletak pada parameternya, dimana _method setMusic tanpa parameter_ digunakan untuk menginisialisasi data music berdasarkan input user dan _method setMusic dengan parameter_ digunakan untuk menginisialisasi data music dari sisi pengembang/programmer

        line: 160-233

        link: [overloading method setMusic](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L160)

    - Method setPodcast() pada class Data

        [**setPodcast()**](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L238)
        [**setPodcast()**](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L296)


        **setPodcast(String tittle, String artist, String genre, int epsCount)**

        kasus overloading method ini hampir sama dengan method _setMusic_ yaitu terletak pada parameternya, dimana method _setPodcast tanpa parameter_ digunakan untuk menginisialisasi data music berdasarkan input user dan method _setPodcast dengan parameter_ digunakan untuk menginisialisasi data music dari sisi pengembang/programmer

        line: 235-309

        link: [overloading method setPodcast](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L235)

# No 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

proses bisnis:


| No.  | UseCase |method yang terkait|
| ---- | ------- |-------------------|
|1.|user dapat signUp apabila belum memiliki akun|class Display: [signUpSection](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Display.java#L64)|
|2.|user dapat melakukan login aplikasi|class Display: [signInSection()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Display.java#L80)|
|3.|user mendapatkan validasi data login|class Display: [signInSection()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Display.java#L80)|
|4.|aplikasi menyapa user berdasarkan waktu sistem|class Additional: [getGreetings()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Additional.java#L33)|
|5.|user dapat melihat daftar riwayat pemutaran konten|class Display: [listHistorySection()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Display.java#L215); [listAllContent("history")](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Display.java#L250)|
|6.|user dapat memutar konten|class Features: [MusicPlayer.play()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L62); [PodcastPlayer.play()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L143)|
|7.|user dapat menjeda pemutaran konten|class Features: [MusicPlayer.pause()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L76); [PodcastPlayer.pause()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L158)|
|8.|user dapat melihat daftar musik yang tersedia|class Display: [listAllContent("music")](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Display.java#L250)|
|9.|user dapat melihat daftar podcast yang tersedia|class Display: [listAllContent("podcast")](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Display.java#L250)|
|10.|user dapat berpindah ke konten sebelum atau sesudahnya|class MusicPlayer: [next()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L90); [prev()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L100); class PodcastPlayer: [next()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L174); [prev()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Features.java#L203)|
|11.|program dapat menyimpan data riwayat konten user|class Data: [writeHistory()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L557)|
|12.|program dapat menerima input data konten(tambahan)|class Data: [setMusic()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L163); [setPodcast()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L238)|
|13.|program dapat menyimpan data ke database(tambahan)|class Data: [writeUser()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L395); [writeCondition()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L439); [writeMusic()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L479); [writePodcast()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L518)|
|14.|program dapat membaca data dari file database dan menggunakannya kembali(tambahan)|class Data: [readUser()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L416); [readCondition()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L458); [readMusic()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L497); [readPodcast()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L536); [readHistory()](https://gitlab.com/rihapfirdaus/spotifai/-/blob/main/Data.java#L575)|

Untuk mendukung jalannya program saya menambahkan beberapa proses diluar aplikasi spotify seperti menginisialisasi data konten,menambahkan data konten dan menyimpannya ke database dalam bentuk file dat. Kemudian program juga memungkinkan untuk membaca data dalam file dat tersebut lalu mengubahnya menjadi data yang dapat digunakan kembali dalam program. 
# No 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

Class Diagram:
[![Class Diagram spotifai](https://mermaid.ink/img/pako:eNrNWMFu4zYQ_RVCJ6lr9wOEIECw6e4GiLsBsrn5wkhTh61ECeRoN0Y2-fYOKcomKUpNe2oOjvzmzXDecEiRfsmqroaszKqGa30t-EHxljG2l_TBLMh2XMiXETB_H1r6nhcj8OoTPwHHQYF-Odl8613Dj6C8QBcX_FGj4hVeXnrhFVQgMReyhueSCYnFL561pyiLNj5oWDJKeF4OquB7whYL6OqKa5zp-HClFD_eCo3siT46dUza-tF9VemK0DWdKzJXVCZmcDdoUf1Xga1x_h_KCwSCHHxlGrnCvIi627ACsOtRdHIsTMKw6xQEcA3f_RhBja_qWhgn3viJVA1wdV8pABlEOgB-JhCFPOjAoKHnitNkBKiQ_YA7oVuO1VOUkqk4ihZmlbNjf1Jdeysk5JZoORvWEDCjt6A1P4SC4VngLsSjyl9z5F8f_3yZ0KAmHzuJ1CteQbaiLtnDw821B6FAbCife1RUDs9Ac0gNmDAcQKqEAxUPv9lgeRgzLDBe2bjrnM9miBUKTeBNnRexGIO7FIpEgofT4AvWcdjIOF_LfkV58zi0iSIJ_bFrGqhMU5bsseuoHWRUCOOaBwGiMpxD5Ml4UUnGgAvavFhFlNBrYkP2NUJPgw8SbdOG-C3XMWwS_8155IFrMSMZ99wPEyk6xSmM1VuFM5qNVHipBKK-jDvsPy2FYw9jJvPpNFvggthvzi03_imhN5PzwiZ78GIszN45RKQx3g18ibTjK8nb1OJegHsK86NTdcJEI1APyXGbTdjtyijZ6c315vamX63hzWO6Dktxnclnu6lLsZ3pLaz2A6nOY-kbFn6LhUYzZnPOk2C4KW1YuE1uWLA7knlleTu1-QL8L0eCXmg6dqYa0BXq3KAblu5Eu-e4OaZmRVZN33zW0NccwS26KVvDtkFNbGRuUUdt_uCmZanNf1-x3bkpW7K7OXtPC9pwU_Hf14fGZSpj8b5m3P5QAsG2o1eGrQJez0BLPZc-5ptXeYxbl1mjWvocteREv1l6CrcOJ8GRQwJ3TeHOHMkz2rXQ5oDqH9CeKBio--m95J99JKrj136Ga3GQQqY8jGXoU5bxzGwOmilrI-zGZtJeMju9S-arppnrHh_MNY9tt_aI7BB6Msh0rwvR8zk2xF3tIpBab0SmYKkQnu3npbswstKsbKTk3PgOvvhpkz1fWFhJdwOQdUwjVnBzi3h-hCDR0CkwOYkpCW6NjQpcqWcSApJNYJ3iUlknuZmfkaYkbCWmwYISBIzzWAHHDHUqeAjd-RfbCfziXxQnMCpWtslaUHThqrMys2ttn-ETtLDPSnqsufprn-3lK_H4gN39UVZZiWqATTYuYfd7RVb-wRtNKFD4Tu3cLxrm3-vfiVEneg?type=png)](https://mermaid-js.github.io/mermaid-live-editor/edit#pako:eNrNWMFu4zYQ_RVCJ6lr9wOEIECw6e4GiLsBsrn5wkhTh61ECeRoN0Y2-fYOKcomKUpNe2oOjvzmzXDecEiRfsmqroaszKqGa30t-EHxljG2l_TBLMh2XMiXETB_H1r6nhcj8OoTPwHHQYF-Odl8613Dj6C8QBcX_FGj4hVeXnrhFVQgMReyhueSCYnFL561pyiLNj5oWDJKeF4OquB7whYL6OqKa5zp-HClFD_eCo3siT46dUza-tF9VemK0DWdKzJXVCZmcDdoUf1Xga1x_h_KCwSCHHxlGrnCvIi627ACsOtRdHIsTMKw6xQEcA3f_RhBja_qWhgn3viJVA1wdV8pABlEOgB-JhCFPOjAoKHnitNkBKiQ_YA7oVuO1VOUkqk4ihZmlbNjf1Jdeysk5JZoORvWEDCjt6A1P4SC4VngLsSjyl9z5F8f_3yZ0KAmHzuJ1CteQbaiLtnDw821B6FAbCife1RUDs9Ac0gNmDAcQKqEAxUPv9lgeRgzLDBe2bjrnM9miBUKTeBNnRexGIO7FIpEgofT4AvWcdjIOF_LfkV58zi0iSIJ_bFrGqhMU5bsseuoHWRUCOOaBwGiMpxD5Ml4UUnGgAvavFhFlNBrYkP2NUJPgw8SbdOG-C3XMWwS_8155IFrMSMZ99wPEyk6xSmM1VuFM5qNVHipBKK-jDvsPy2FYw9jJvPpNFvggthvzi03_imhN5PzwiZ78GIszN45RKQx3g18ibTjK8nb1OJegHsK86NTdcJEI1APyXGbTdjtyijZ6c315vamX63hzWO6Dktxnclnu6lLsZ3pLaz2A6nOY-kbFn6LhUYzZnPOk2C4KW1YuE1uWLA7knlleTu1-QL8L0eCXmg6dqYa0BXq3KAblu5Eu-e4OaZmRVZN33zW0NccwS26KVvDtkFNbGRuUUdt_uCmZanNf1-x3bkpW7K7OXtPC9pwU_Hf14fGZSpj8b5m3P5QAsG2o1eGrQJez0BLPZc-5ptXeYxbl1mjWvocteREv1l6CrcOJ8GRQwJ3TeHOHMkz2rXQ5oDqH9CeKBio--m95J99JKrj136Ga3GQQqY8jGXoU5bxzGwOmilrI-zGZtJeMju9S-arppnrHh_MNY9tt_aI7BB6Msh0rwvR8zk2xF3tIpBab0SmYKkQnu3npbswstKsbKTk3PgOvvhpkz1fWFhJdwOQdUwjVnBzi3h-hCDR0CkwOYkpCW6NjQpcqWcSApJNYJ3iUlknuZmfkaYkbCWmwYISBIzzWAHHDHUqeAjd-RfbCfziXxQnMCpWtslaUHThqrMys2ttn-ETtLDPSnqsufprn-3lK_H4gN39UVZZiWqATTYuYfd7RVb-wRtNKFD4Tu3cLxrm3-vfiVEneg)

- UseCase table:

|No.   |Lingkup UseCase|UseCase|Ket|
| ---- | ------------- |-------|---|
|1.|Proses masuk|user dapat signUp apabila belum memiliki akun|sudah|
|2.||user dapat melakukan login aplikasi|sudah|
|3.||user mendapatkan validasi data login|sudah|
|4.||user dapat mengubah data login|-|
|5.|Konten|user dapat melihat daftar riwayat pemutaran konten|sudah|
|6.||user dapat memutar konten|sudah|
|7.||user dapat menjeda pemutaran konten|sudah|
|8.||user dapat mendengarkan konten|-|
|9.||user dapat melihat detail konten|-|
|10.||user dapat mencari konten|-|
|11.||user dapat menambahkan konten ke playlist|-|
|12.||user dapat membagikan konten|-|
|13.||user dapat memindai konten dari barcode khas aplikasi|-|
|14.||user dapat mengatur timer waktu tidur|-|
|15.|Musik| user dapat melihat daftar musik yang tersedia|sudah|
|16.|| user dapat memberi tanda suka pada musik|-|
|17.|| user dapat menyembunyikan musik|-|
|18.|| user dapat memutar musik secara acak/berurutan/berulang|-|
|19.|| user dapat berpindah ke lagu sebelum atau sesudahnya |sudah|
|20.|| user dapat melihat lirik musik apabila tersedia|-|
|21.|Podcast| user dapat melihat daftar podcast yang tersedia|sudah|
|22.|| user dapat mengatur kecepatan dalam pemutaran podcast|-|
|23.|| user dapat berpindah ke 15 menit sebelum atau sesudahnya saat memutar podcast|-|
|24.|| user dapat mendownload podcast|-|
|25.|| user dapat menandai podcast yang telah diputar|-|
|26.|Subscription| user dapat beralih ke akun premium|-|
|27.|| user dapat membatalkan langganan akun premium|-|
|28.|| user dapat memilih jenis akun premium yang akan dibeli|-|
|29.|| user dapat memilih metode pembayaran|-|
|30.|Akun premium| user akan mendapatkan iklan disela-sela pemutaran konten|-|
|31.|| user tidak bisa menggunakan aplikasi secara offline|-|
|32.|| user tidak bisa bebas dalam memutar konten|-|
|33.|Akun non premium| user tidak mendapat iklan|-|
|34.|| user dapat menggunakan aplikasi secara offline|-|
|35.|| user dapat bebas memutar konten|-|
|36.|Data|aplikasi dapat menyimpan data user|sudah|
|37.||aplikasi dapat menyimpan data riwayat konten user|sudah|
|38.||aplikasi dapat menyimpan preferensi pemutaran konten user|-|
|39.||aplikasi dapat menyimpan data konten|sudah|
|40.|profile|aplikasi dapat menyimpan data profile|-|
|40.||user dapat merubah profile|-|
|40.||user dapat keluar dari profile|-|
|40.||user dapat berpindah profile|-|

# No 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

[demonstrasi projek](https://youtu.be/TwtSYak7H6U)

# No 10
Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

![UX&shortdemo](https://gitlab.com/rihapfirdaus/uts-praktikum-pbo/-/raw/main/shortdemo.gif)
